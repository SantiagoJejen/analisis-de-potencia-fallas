classdef lineas
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        z0
        z1
        z2
        barra1
        barra2
    end
    
    methods
        function BA=lineas(z0,z1,z2,barra1,barra2)
            BA.z0=z0;
            BA.z1=z1;
            BA.z2=z2;
            BA.barra1=barra1;
            BA.barra2=barra2;
        end
    end
    
end

