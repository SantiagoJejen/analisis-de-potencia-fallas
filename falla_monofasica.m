function [ Vbarra_fase,IF ] = falla_monofasica( Zbus_0,Zbus_1,Zbus_2,barraFalla,VF,ZF)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%Definicion de a
a=complex(cosd(120),sind(120));
%Hacer la matriz A
A=[1 1 1;
   1 a^2 a 
   1 a a^2];
%hallar la corriente Iao
Ia0=VF/(Zbus_0(barraFalla,barraFalla)+Zbus_1(barraFalla,barraFalla)+Zbus_2(barraFalla,barraFalla)+ZF);


fprintf('\nIa0 pu= %.4f < %.4f pu\n',abs(Ia0),angle(Ia0)*(180/pi))
IF=3*Ia0;
fprintf('\nIF pu= %.4f < %.4f pu\n',abs(Ia0),angle(Ia0)*(180/pi))
Ia1=Ia0;
Ia2=Ia0;
IS=[Iao;Ia1;Ia2];
disp(table(IS))
Ibarra=A*IS;
disp(table(Ibarra,A,IS))
Vs_fase=[0;VF;0]-[Zbus_0(barraFalla,barraFalla) 0 0; 0 Zbus_1(barraFalla,barraFalla) 0 ; 0 0 Zbus_2(barraFalla,barraFalla)]*[Iao;Ia1;Ia2];
disp(table(Vs_fase,[0;VF;0],[Zbus_0(barraFalla,barraFalla) 0 0; 0 Zbus_1(barraFalla,barraFalla) 0 ; 0 0 Zbus_2(barraFalla,barraFalla)],[Iao;Ia1;Ia2]))
Vbarra_fase = A*Vs_fase;

end

