classdef transformafores
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
      %  potencia
      %  voltaje_alta
      %  voltaje_baja
        x0
        x1
        x2
      %  coneccion 
      barra1
      barra2
    end
    methods
        function BA=transformafores(x0, x1, x2, barra1,barra2)
            BA.x0=x0;
            BA.x1=x1;
            BA.x2=x2;
            BA.barra1=barra1;
            BA.barra2=barra2;
        end
    end
    
end

