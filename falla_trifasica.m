function [ voltajesBarras,corrienteFalla ] = falla_trifasica( Zbus,barraFalla,voltajePrefalla )
%UNTITLED Summary of this function goes here
%   Calcular los voltaje en las barras y la corriente de falla 

corrienteFalla=voltajePrefalla/Zbus(barraFalla,barraFalla);
    for i=1:length(Zbus)
        voltajesBarras(i)=voltajePrefalla*(1-Zbus(i,barraFalla)/Zbus(barraFalla,barraFalla));
    end
end

