function varargout = VentanaTexto(varargin)
% VENTANATEXTO MATLAB code for VentanaTexto.fig
%      VENTANATEXTO, by itself, creates a new VENTANATEXTO or raises the existing
%      singleton*.
%
%      H = VENTANATEXTO returns the handle to a new VENTANATEXTO or the handle to
%      the existing singleton*.
%
%      VENTANATEXTO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VENTANATEXTO.M with the given input arguments.
%
%      VENTANATEXTO('Property','Value',...) creates a new VENTANATEXTO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before VentanaTexto_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to VentanaTexto_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help VentanaTexto

% Last Modified by GUIDE v2.5 09-Sep-2019 11:13:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @VentanaTexto_OpeningFcn, ...
                   'gui_OutputFcn',  @VentanaTexto_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before VentanaTexto is made visible.
function VentanaTexto_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to VentanaTexto (see VARARGIN)

% Choose default command line output for VentanaTexto
handles.output = hObject;
set(handles.text2,'String',varargin);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes VentanaTexto wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = VentanaTexto_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pause();
% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;
