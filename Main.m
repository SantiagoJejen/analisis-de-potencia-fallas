%% ---------------------------------------------------------------------------------------------------------------------------------
%   Escuela Colombiana De ingenieria Julio Garavito
%   Sistema de potencia 2
%   --
%   Maria Fernanada Sandoval
%   Santiago Jejen Salinas 
%---------------------------------------------------------------------------------------------------------------------------------

%% Limpiar el WorkSpace
clear all;
close all;
clc

%% Solicitud de n�mero de barras;
%Ventanas(text);
disp('Ingrese el numero de barras del sistema')
N=input('= ');
while N==0 || N<0
   disp('Ingrese un valor correcto de numero de barras:');
   N=input('= ');
end
Y=zeros(N);
Z=zeros(N);

%% Crear el vector de voltajes
for ii=1:N
    text= ['v',num2str(ii)];
    FV(1,ii)={text};
end

%% Crear lineas 
lines=[lineas(0.0056710775047259 + i*0.0425330812854442, 0.00226843100189036 + i*0.0141776937618147,0.00226843100189036 + i*0.0141776937618147,2,3)
       lineas(0.0189035916824197 + i*0.141776937618147,  0.00756143667296786 + i*0.0472589792060491,0.00756143667296786 + i*0.0472589792060491,3,4)
       lineas(0.0151228733459357 + i*0.113421550094518,  0.00604914933837429 + i*0.0378071833648393,0.00604914933837429 + i*0.0378071833648393,4,5)
       lineas(0.0189035916824197 + i*0.141776937618147,  0.00756143667296786 + i*0.0472589792060491,0.00756143667296786 + i*0.0472589792060491,4,6)
       lineas(0.0056710775047259 + i*0.0425330812854442, 0.00226843100189036 + i*0.0141776937618147,0.00226843100189036 + i*0.0141776937618147,5,6)
        ];
%% Crear Transformadores 
trafos=[transformafores(0.1*i,0.1*i,0.1*i,1,2)
        transformafores(0.05*i,0.05*i,0.05*i,6,7)
        ];  
    
%% Crear generadores 
genes=[generadores(0.05*i,0.12*i,0.14*i,1)
       generadores(0.025*i,0.06*i,0.07*i,7)
       ];


%% sacar la matriz y bus
for ii=1:N
    for j=1:N
        for p=1:length(lines)
            if ii==j && (lines(p).barra1==ii || lines(p).barra2==ii )
                Y(ii,ii)= Y(ii,ii) + (lines(p).z1)^-1;
            elseif lines(p).barra1==ii && lines(p).barra2==j && ii~=j 
                Y(ii,j)= Y(ii,j) + (lines(p).z1)^-1;
                Y(j,ii)=Y(ii,j);
            end
        end
        for e=1:length(trafos)
            if ii==j && (trafos(e).barra1==ii || trafos(e).barra2==ii)
                Y(ii,ii)= Y(ii,ii) + (trafos(e).x1)^-1;        
            elseif trafos(e).barra1==ii && trafos(e).barra2==j && ii~=j 
                Y(ii,j)= Y(ii,j) + (trafos(e).x1)^-1;
                Y(j,ii)= Y(ii,j);
            end 
         end   
    end
end

%%
%Para los generadores
for ii=1:length(genes)
    Y(genes(ii).barra,genes(ii).barra)=Y(genes(ii).barra,genes(ii).barra)+(genes(ii).x1)^-1;
end
for ii=1:N
    for j=1:N
        if ii~=j
            Y(ii,j)= -Y(ii,j);
        end
    end
end

%% Resultado
disp('La  matriz de admitancias es: ')
% TablaAdmitancia = table(FC',Y,FV')
TablaAdmitancia = table(Y)



%% Calcular el valor de la inversa Impedancias
  Z=inv(Y);
  disp('La matriz de impedancias es: ')
  TablaImpedancias = table(Z)
%  TablaImpedancias = table(FV',Z,FC')
%  VentanaTexto(num2str(Y))
% clearvars

 
 %% Pedir barra fallada
% disp('Ingrese la barra donde tiene la falla')
% barraFalla=input('= ');
% disp('Ingrese el valor del voltaje prefalla')
% voltajePrefalla=input('= ');
% [V,If]=falla_trifasica( Z,barraFalla,voltajePrefalla );
% disp('Voltajes ')
% V
% disp('Corriente de Falla  ')
% If



 